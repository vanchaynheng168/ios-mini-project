//
//  ArticleViewController.swift
//  MINI-PROJECT-IOS
//
//  Created by Nheng Vanchhay on 12/10/20.
//

import UIKit
import Kingfisher

class ArticleViewController: UIViewController, SentArticleDelegate {
  
    @IBOutlet weak var desArticle: UITextView!
    @IBOutlet weak var titleArticle: UILabel!
    @IBOutlet weak var imagePost: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    var id: String = ""
    var titleArt: String = ""
    var image: String = ""
    var des: String = ""
    var dateArticle: String = ""
    var author: String = ""
    var profileAuthor: String = ""
    
    var updateDataDelete: SentArticleDelegate?
    
    let network = Service.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleArticle.text = titleArt
        self.imagePost.kf.setImage(with: URL(string: image), placeholder: UIImage(named: "Image"), options: nil, progressBlock: nil) { result in
                switch result {
                case .success( _):
                    break
                case .failure(_):
                        self.imagePost.image = UIImage(named: "defaultImage")
                }
            }
        self.profileImage.image = UIImage(named: profileAuthor)
        
        self.date.text = dateArticle
        self.desArticle.text = des
        self.authorLabel.text = author
        self.desArticle.isEditable = false
        
    }
    
    func sentArticle(id: String ,image: String, title: String, description: String, date: String, author: String, authorProfile: String, action: String) {
        self.id = id
        self.image = image
        self.titleArt = title
        self.des = description
        self.dateArticle = date
        self.author = author
        self.profileAuthor = authorProfile
    }
    @IBAction func deletePress(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Do you want to delete this article?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            self.network.deleteMethod(id: self.id, completion: { message in
                let alert = UIAlertController(title: "", message: message.message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            })
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func updatePress(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(identifier: "AddUpdateArticleScreen") as! CrudArticleViewController
            self.updateDataDelete = vc
            self.updateDataDelete?.sentArticle(id: id,image: image, title: titleArt, description: des, date: "date test", author: author, authorProfile: profileAuthor, action: "update")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
