//
//  ViewController.swift
//  MINI-PROJECT-IOS
//
//  Created by Nheng Vanchhay on 12/8/20.
//

import UIKit

class ViewController: UIViewController, UINavigationControllerDelegate,UIScrollViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var searchBarArticle: UISearchBar!
    @IBOutlet weak var table: UITableView!
    var netWork =  Service.shared
    var articles = [ArticleModel]()
    let modelName = ["Kingfisher", "Alamofire", "URL Session", "Swift UI", "StoryBord", "Hero"]
    let imageProfileName = ["dargon", "person", "x-man", "Baymax", "spider-man"]
    var sentDelegate: SentArticleDelegate?
    
    var edit: String = "edit".localizedString()
    var delete: String = "delete".localizedString()
    var share: String = "share".localizedString()
    var date: String = "date".localizedString()
    var category: String = "category".localizedString()
    var messageDelete: String = "messageDelete".localizedString()
    var page = [Int]()
    var filterTitle: String = ""
    var fetchPageMore = 1
    var isFetchMore: Bool = false
    var isSearch: Bool = false
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.register(TableViewCell().nib(), forCellReuseIdentifier: TableViewCell.identifirecell)
        fetchData()
        //append data for filter
        searchBarArticle.delegate = self
        //Refresh Data
        table.refreshControl = UIRefreshControl()
        table.refreshControl?.addTarget(self, action: #selector(didPullRefresh), for: .valueChanged)
        
        self.searchBarArticle.layer.borderWidth = 0
        self.searchBarArticle.barTintColor = #colorLiteral(red: 0.9318010211, green: 0.9319538474, blue: 0.9317685962, alpha: 1)
        
        if let textfield = searchBarArticle.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = UIColor.white
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "Placeholder", attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray])
            
            if let leftView = textfield.leftView as? UIImageView {
                leftView.image = leftView.image?.withRenderingMode(.alwaysTemplate)
                leftView.tintColor = UIColor.gray
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.edit = "edit".localizedString()
        self.delete = "delete".localizedString()
        self.share = "share".localizedString()
        self.category = "date".localizedString()
        self.date = "category".localizedString()
        self.messageDelete = "messageDelete".localizedString()
        self.navigationItem.title = "titleMainScreen".localizedString()
        self.category = "category".localizedString()
    }
   
    @objc func didPullRefresh() {
        isFetchMore = false
        fetchData()
        DispatchQueue.main.async {
            self.table.refreshControl?.endRefreshing()
        }
    }
    //getData
    func fetchData (filterTitler: String? = "", fetchPage: Int? = 1) {
        let loading = UIActivityIndicatorView(frame: CGRect(x: self.view.bounds.width*0.5, y: self.view.bounds.height*0.5, width: 0, height: 0))
        let transfrom = CGAffineTransform.init(scaleX: 2.2, y: 2.2)
        loading.transform = transfrom
        self.view.addSubview(loading)
        loading.startAnimating()
        
        ViewModel.shared.fetchData(title: filterTitler!, fetchPage: fetchPage!, completion: { (article, getPage)  in
            self.page = getPage
            if self.isFetchMore == true {
                self.articles.append(contentsOf: article)
            }else{
                self.articles = article
            }
            DispatchQueue.main.async {
                self.table.reloadData()
                loading.stopAnimating()
            }
        })
    }
    //searchbar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let titleSearch = searchText
        if searchText == ""{
            self.isSearch = false
            fetchData()
        }else{
            self.isSearch = true
            fetchData(filterTitler: titleSearch)
        }
    }
    //fetch more page
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = articles.count - 1
        if lastItem == indexPath.row{
            if page[1] > page[0] {
                print("No Page")
                return
            }else{
                if isSearch {
                   return
                }else{
                    isFetchMore = true
                    fetchPageMore = fetchPageMore + 1
                    fetchData(fetchPage: fetchPageMore)
                }
               
            }
        }
    }
    //push data to view
    func sentData(indexPath: Int, action: String) {
        let vc = storyboard?.instantiateViewController(identifier: "screenArticle") as! ArticleViewController
        let vcEdit = storyboard?.instantiateViewController(identifier: "AddUpdateArticleScreen") as! CrudArticleViewController
        if action != "update"{
            self.sentDelegate = vc
            self.sentDelegate?.sentArticle(id: articles[indexPath].id,image: articles[indexPath].image ?? "", title: articles[indexPath].title, description: articles[indexPath].dataDescription, date: articles[indexPath].createdAt!, author: articles[indexPath].author, authorProfile: articles[indexPath].profileAuthor, action: action)
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.sentDelegate = vcEdit
            self.sentDelegate?.sentArticle(id: articles[indexPath].id,image: articles[indexPath].image ?? "", title: articles[indexPath].title, description: articles[indexPath].dataDescription, date: articles[indexPath].createdAt!, author: articles[indexPath].author, authorProfile: articles[indexPath].profileAuthor, action: action)
            self.navigationController?.pushViewController(vcEdit, animated: true)
        }
        
    }
}
extension ViewController: UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sentData(indexPath: indexPath.row, action: "save")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifirecell, for: indexPath) as! TableViewCell
        cell.config(title: articles[indexPath.row].title, createDate: articles[indexPath.row].createdAt!, image: articles[indexPath.row].image ?? "", category: articles[indexPath.row].category, cate: category, author: articles[indexPath.row].author, profileAuthor: articles[indexPath.row].profileAuthor)
        
        return cell
    }
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let shareAction = UIAction(title: self.share, image: UIImage(systemName: "square.and.arrow.up")) { _ in
            print("Share")
        }
        let edit = UIAction(title: self.edit, image: UIImage(systemName: "pencil.circle.fill")) { _ in
            self.sentData(indexPath: indexPath.row, action: "update")
        }
        let delete = UIAction(title: self.delete, image: UIImage(systemName: "trash.circle.fill")) { _ in
            //waiting loading
            let loading = UIActivityIndicatorView(frame: CGRect(x: self.view.bounds.width*0.5, y: self.view.bounds.height*0.5, width: 0, height: 0))
            let transfrom = CGAffineTransform.init(scaleX: 2.2, y: 2.2)
            loading.transform = transfrom
            self.view.addSubview(loading)
            loading.startAnimating()
            
            let alert = UIAlertController(title: "", message: self.messageDelete, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.netWork.deleteMethod(id: self.articles[indexPath.row].id, completion: { message in
                    let alert = UIAlertController(title: "", message: message.message, preferredStyle: .alert)
                    //stop loading
                    loading.stopAnimating()
                    alert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                })
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        // Pass the indexPath as the identifier for the menu configuration
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            return UIMenu(title: "", children: [edit, delete, shareAction])
        }
    }
    private func createSpinenrFooter() -> UIView{
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50))
        footerView.backgroundColor = #colorLiteral(red: 0.9318010211, green: 0.9319538474, blue: 0.9317685962, alpha: 1)
        let spinner = UIActivityIndicatorView()
        let transfrom = CGAffineTransform.init(scaleX: 1.6, y: 1.6)
        spinner.transform = transfrom
        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()
        return footerView
    }
   
}
