//
//  SetLanguageViewController.swift
//  MINI-PROJECT-IOS
//
//  Created by KSGA-004 on 12/15/20.
//

import UIKit

class SetLanguageViewController: UIViewController {
    @IBOutlet weak var languageSwift: UILabel!
    @IBOutlet weak var totalPost: UILabel!
    @IBOutlet weak var chooseColor: UILabel!
    @IBOutlet weak var logOut: UIButton!
    @IBOutlet weak var swiftLangLabel: UISwitch!
    let network = AppService.shared
    
    var setLangDelegate: SetLanguageDeletgate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("AppService.shared.language",AppService.shared.language)
        if AppService.shared.language == "en"{
            swiftLangLabel.isOn = true
        }else{
            swiftLangLabel.isOn = false
        }
        setLanguageSwift(language: "language".localizedString(), totalPost: "totalPost".localizedString(), chooseColor: "chooseColor".localizedString(), logout: "buttonLogout".localizedString())
        // Do any additional setup after loading the view.
    }
    func setLanguage(language: String) {
        network.choose(language: language)
    }

    func setLanguageSwift(language: String, totalPost:String, chooseColor: String, logout: String) {
        self.languageSwift.text = language
        self.totalPost.text = totalPost
        self.chooseColor.text = chooseColor
        self.logOut.setTitle(logout, for: .normal)
    }
    @IBAction func LoginPress(_ sender: Any) {
        
    }
    @IBAction func SwitchLang(_ sender: UISwitch) {
        
        if sender.isOn == true {
            self.setLanguage(language: language.english.rawValue)
            self.setLanguageSwift(language: "language".localizedString(), totalPost: "totalPost".localizedString(), chooseColor: "chooseColor".localizedString(), logout: "buttonLogout".localizedString())
            self.setLangDelegate?.setLang(setLang: SetLangStruct(title: "titleMainScreen".localizedString(), date: "date".localizedString(), category: "category".localizedString(), edit: "edit".localizedString(), delete: "delete".localizedString(), share: "share".localizedString()))
           
        }else{
            self.setLanguage(language: language.khmer.rawValue)
            self.setLanguageSwift(language: "language".localizedString(), totalPost: "totalPost".localizedString(), chooseColor: "chooseColor".localizedString(), logout: "buttonLogout".localizedString())
            self.setLangDelegate?.setLang(setLang: SetLangStruct(title: "titleMainScreen".localizedString(), date: "date".localizedString(), category: "category".localizedString(), edit: "edit".localizedString(), delete: "delete".localizedString(), share: "share".localizedString()))
        }
    }
}
extension String {
    func localizedString()->String {
        let path = Bundle.main.path(forResource: AppService.shared.language, ofType: "lproj")!
        let bundle = Bundle(path: path)!
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: self, comment: self)
    }
}

