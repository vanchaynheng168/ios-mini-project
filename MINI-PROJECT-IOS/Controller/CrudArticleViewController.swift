//
//  AddArticleViewController.swift
//  MINI-PROJECT-IOS
//
//  Created by Nheng vanchhay on 12/11/20.
//

import UIKit
import Kingfisher

class CrudArticleViewController: UIViewController, SentArticleDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var saveLabel: UIButton!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var imageADD: UIImageView!{
        didSet {
            imageADD.isUserInteractionEnabled = true
        }
    }
    @IBOutlet weak var titleArticle: UITextView!
    @IBOutlet weak var descriptionArticle: UITextView!
    
    var imagePicker = UIImagePickerController()
    
    var id: String = ""
    var image: String = ""
    var titleArt: String = ""
    var desArticle: String = ""
    var action: String = ""
    var imageSelected: Bool = false
    
    let network = Service.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageResult = image.contains(".png") || image.contains(".jpg") || image.contains(".jpeg")
        if imageResult {
            let url = URL(string: image)
            self.imageADD.kf.setImage(with: url)
        }else{
            self.imageADD.image = UIImage(named: "defaultImage")
        }
        self.titleArticle.text = titleArt
        self.descriptionArticle.text = desArticle
        
        if action != "update"{
            title = "Add Article"
        }else{
            title = "Update Article"
        }
        // Initialize Tap Gesture Recognizer
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapImageView(_:)))
        // Add Tap Gesture Recognizer
        imageADD.addGestureRecognizer(tapGestureRecognizer)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.title = ""
        if action != "update"{
            self.navigationItem.title = "addArticle".localizedString()
            self.saveLabel.setTitle("save".localizedString(), for: .normal)
        }else{
            self.navigationItem.title = "updateArticle".localizedString()
            self.saveLabel.setTitle("update".localizedString(), for: .normal)
        }
        self.titleLabel.text = "titleArticle".localizedString()
        self.desLabel.text = "description".localizedString()
    }
    // MARK: - Actions
    
    @objc private func didTapImageView(_ sender: UITapGestureRecognizer) {
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.delegate = self
        self.present(self.imagePicker,animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            self.imageADD.image = image
            imageSelected = true
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    //fun post data
    func uploadArticle(image: String, title: String, description: String) {
        ViewModel.shared.postMethod(image: image, title: title, description: description, completion: { (message) in
            let alert = UIAlertController(title: "", message: message.message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Done", style: .default, handler: {_ in
                self.titleArticle.text = ""
                self.titleArticle.becomeFirstResponder()
                self.descriptionArticle.text = ""
                self.imageADD.image = UIImage(named: "defaultImage")
                self.imageSelected = false
            }))
            self.present(alert, animated: true, completion: nil)
        } )
    }
    //fun Update Data
    func updateData(id: String,image: String, title: String, description: String) {
        ViewModel.shared.putMethod(id: id, image: image, title: title, description: description, completion: { (message) in
            let alert = UIAlertController(title: "", message: message.message, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        })
    }
    @IBAction func uploadPress(_ sender: Any) {
        guard titleArticle.text != "" else {
            return
        }
        //waiting loading
        let loading = UIActivityIndicatorView(frame: CGRect(x: self.view.bounds.width*0.5, y: self.view.bounds.height*0.5, width: 0, height: 0))
        let transfrom = CGAffineTransform.init(scaleX: 2.2, y: 2.2)
        loading.transform = transfrom
        self.view.addSubview(loading)
        loading.startAnimating()
        
        if imageSelected {
            self.network.uploadImage(image: imageADD.image!, completion: { imageResponse in
                if self.action != "update"{
                    self.uploadArticle(image: imageResponse.url, title: self.titleArticle.text, description: self.descriptionArticle.text)
                    //stop loading
                    loading.stopAnimating()
                }else{
                    self.updateData(id: self.id, image: imageResponse.url, title: self.titleArticle.text, description: self.descriptionArticle.text)
                    //stop loading
                    loading.stopAnimating()
                }
            })
        }else{
            if self.action != "update"{
                self.uploadArticle(image: "String", title: self.titleArticle.text, description: self.descriptionArticle.text)
                //stop loading
                loading.stopAnimating()
            }else{
                self.updateData(id: self.id, image: image, title: self.titleArticle.text, description: self.descriptionArticle.text)
                //stop loading
                loading.stopAnimating()
            }
        }
    }
    //delegate sent data
    func sentArticle(id: String, image: String, title: String, description: String, date: String, author: String, authorProfile: String, action: String) {
        self.id = id
        self.image = image
        self.titleArt = title
        self.desArticle = description
        self.action = action
    }
}
