//
//  AppService.swift
//  MINI-PROJECT-IOS
//
//  Created by KSGA-004 on 12/15/20.
//

import Foundation
import UIKit

struct AppService {
    
    static let shared = AppService()
    
    var language: String {
        return UserDefaults.standard.string(forKey: "lang") ?? "en"
    }
    
    func choose(language: String){
        UserDefaults.standard.set(language, forKey: "lang")
    }
}

enum language: String {
    case english = "en"
    case khmer = "km"
}
