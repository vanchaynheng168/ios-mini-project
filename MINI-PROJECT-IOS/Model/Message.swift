//
//  MessageModel.swift
//  MINI-PROJECT-IOS
//
//  Created by KSGA-004 on 12/11/20.
//

import Foundation

// MARK: - Welcome
struct MessageReponse: Codable {
    let data: DataClass
    let message: String
}

// MARK: - DataClass
struct DataClass: Codable {
    let id, title, dataDescription: String
    let published: Bool?
    let image, createdAt, updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case title
        case dataDescription = "description"
        case published, image, createdAt, updatedAt
        case v = "__v"
    }
}

