//
//  Article.swift
//  MINI-PROJECT-IOS
//
//  Created by Nheng Vanchhay on 12/9/20.
//

import Foundation

// MARK: - DataArticle
struct DataArticle: Codable {
    let data: [Article]
    let page: Int?
    let limit: Int?
    let totalPage: Int?
    let message: String
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case page = "page"
        case limit
        case totalPage = "total_page"
        case message
    }
}
// MARK: - Article
struct Article: Codable {
    let id, title, dataDescription: String?
    let published: Bool?
    let image: String?
    let createdAt: String?
    let updatedAt: String?
    let author: Author?
    let category: Category?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case title = "title"
        case dataDescription = "description"
        case published
        case image
        case createdAt
        case updatedAt
        case author
        case category
        case v = "__v"
    }
}
// MARK: - Author
struct Author: Codable {
    let id: String
    let name: String
    let email: String
    let image: String
    let createdAt: String
    let updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, email, image, createdAt, updatedAt
        case v = "__v"
    }
}

// MARK: - Category
struct Category: Codable {
    let id: String
    let name: String
    let createdAt: String
    let updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, createdAt, updatedAt
        case v = "__v"
    }
}


