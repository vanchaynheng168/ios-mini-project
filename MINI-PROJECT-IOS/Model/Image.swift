//
//  ImageModel.swift
//  MINI-PROJECT-IOS
//
//  Created by KSGA-004 on 12/11/20.
//

import Foundation

// MARK: - Welcome
struct ImageReponse: Codable {
    let id: String
    let url: String
    let createdAt, updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case url, createdAt, updatedAt
        case v = "__v"
    }
}
