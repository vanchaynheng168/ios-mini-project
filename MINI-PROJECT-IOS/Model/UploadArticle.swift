//
//  UploadModelArticle.swift
//  MINI-PROJECT-IOS
//
//  Created by Nheng Vanchhay on 12/10/20.
//

import Foundation

// Create model
struct UploadData: Codable {
    
    let title: String
    let description: String
    let published: Bool
    let image: String
}


