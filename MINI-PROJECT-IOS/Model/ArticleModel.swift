//
//  ArticleModel.swift
//  MINI-PROJECT-IOS
//
//  Created by KSGA-004 on 12/9/20.
//

import Foundation

struct ArticleModel: Codable {
    
    let id, title, dataDescription: String
    let published: Bool
    let image: String?
    var createdAt: String?
    let updatedAt: String
    let author: String
    let profileAuthor: String
    let category: String
    let v: Int?

    init(article: Article) {
        let modelName = ["Kingfisher", "Alamofire", "URL Session", "Swift UI", "StoryBord", "Hero"]
        let imageProfileName = ["dargon", "person", "x-man", "Baymax", "spider-man"]
        let category = ["general", "knowledge","education"]
        self.id = article.id!
        self.title = article.title!
        self.dataDescription = article.dataDescription!
        self.published = article.published ?? true
        self.image = article.image
        self.updatedAt = article.updatedAt!
        self.author = modelName.randomElement()!
        self.profileAuthor = imageProfileName.randomElement()!
        self.category = category.randomElement()!
        self.v = article.v
        
        // formate date
        let dateFormat = DateFormatter()
        // 2020-12-08T11:11:41.945Z
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        if let date = dateFormat.date(from: article.createdAt!) {
            let newDate = DateFormatter()
            newDate.dateFormat = "dd-MMM-yyyy"
            self.createdAt = "Date: \(newDate.string(from: date))"
        }
    }
}
