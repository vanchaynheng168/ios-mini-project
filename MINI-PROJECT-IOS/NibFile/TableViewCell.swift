//
//  TableViewCell.swift
//  MINI-PROJECT-IOS
//
//  Created by KSGA-004 on 12/8/20.
//

import UIKit
import Kingfisher

class TableViewCell: UITableViewCell {

    static let identifirecell: String = "cell"
    @IBOutlet weak var imagePost: UIImageView!
    @IBOutlet weak var catego: UILabel!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var categoryLabeldis: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var createDate: UILabel!
    @IBOutlet weak var profileName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func nib()-> UINib {
        return UINib(nibName: "TableViewCell", bundle: nil)
    }
    func config(title: String, createDate: String, image: String, category: String, cate: String, author: String, profileAuthor: String) {
        
        
        self.title?.text = title
        self.createDate?.text = createDate
        self.profileName.text = author
        self.profile.image = UIImage(named: profileAuthor)
        
        self.imagePost.kf.setImage(with: URL(string: image), placeholder: UIImage(named: "Image"), options: nil, progressBlock: nil) { result in
                switch result {
                case .success( _):
                    break
                case .failure(_):
                        self.imagePost.image = UIImage(named: "defaultImage")
                }
            }
        self.categoryLabeldis.text = category
    }
}
