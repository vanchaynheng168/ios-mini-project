//
//  ProtocolDelegate.swift
//  MINI-PROJECT-IOS
//
//  Created by KSGA-004 on 12/10/20.
//

import Foundation
import UIKit

protocol SentArticleDelegate {
    func sentArticle(id: String, image: String, title: String, description: String, date: String, author: String, authorProfile: String, action: String)
}
struct SetLangStruct {
    var title: String
    var date: String
    var category: String
    var edit: String
    var delete: String
    var share: String
}
protocol SetLanguageDeletgate {
    func setLang(setLang: SetLangStruct)
}
