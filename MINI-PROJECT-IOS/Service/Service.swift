//
//  Service.swift
//  MINI-PROJECT-IOS
//
//  Created by Nheng Vanchhay on 12/9/20.
//

import Foundation
import UIKit

struct Service {
    
    static let shared = Service()
    let baseURL = "http://110.74.194.124:3000/api/articles"
    func fetchArticle(title: String, fetchMorePage: Int,completion: @escaping (DataArticle,Error?)->()) {
        let replaceTitle = title.replacingOccurrences(of: " ", with: "%20")
        guard let url = URL(string: "\(baseURL)?page=\(fetchMorePage)&size=15&title=\(replaceTitle)") else {
            print("url Erorr")
            return
        }
        URLSession.shared.dataTask(with: url) { (data, res, error) in
            if let error = error {
                print("Fail to fetch article : \(error)")
                return
            }
            //Check response
            guard let data = data else {
                print("data nil: ")
                return
            }
            do{
                let article = try JSONDecoder().decode(DataArticle.self, from: data)
                DispatchQueue.main.sync {
                    completion(article, nil)
                }
            }catch let err{
                print("error catch",err)
            }
        }.resume()
    }
    
    // post Article
    func postMethod(articleUpload: UploadData, completion: @escaping(MessageReponse)->()) {
        
            guard let url = URL(string: "\(baseURL)") else {
                print("Error: cannot create URL")
                return
            }
            // Convert model to JSON data
            guard let jsonData = try? JSONEncoder().encode(articleUpload) else {
                print("Error: Trying to convert model to JSON data")
                return
            }
            // Create the url request
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type") // the request is JSON
            request.setValue("application/json", forHTTPHeaderField: "Accept") // the response expected to be in JSON format
            request.httpBody = jsonData
            URLSession.shared.dataTask(with: request) { data, response, error in
                guard error == nil else {
                    print("Error: error calling POST")
                    print(error!)
                    return
                }
                guard let data = data else {
                    print("Error: Did not receive data")
                    return
                }
                guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                    print("Error: HTTP request failed")
                    return
                }
                do{
                    let messageReponse = try JSONDecoder().decode(MessageReponse.self, from: data)
                    DispatchQueue.main.sync {
                        completion(messageReponse)
                    }
                }catch let err{
                    print("error catch",err)
                }
            }.resume()
        }
    //delete
    func deleteMethod(id: String, completion: @escaping(MessageReponse)->()) {
            guard let url = URL(string: "\(baseURL)/\(id)") else {
                print("Error: cannot create URL")
                return
            }
            // Create the request
            var request = URLRequest(url: url)
            request.httpMethod = "DELETE"
            URLSession.shared.dataTask(with: request) { data, response, error in
                guard error == nil else {
                    print("Error: error calling DELETE")
                    print(error!)
                    return
                }
                guard let data = data else {
                    print("Error: Did not receive data")
                    return
                }
                guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                    print("Error: HTTP request failed")
                    return
                }
                do{
                    let messageReponse = try JSONDecoder().decode(MessageReponse.self, from: data)
                    DispatchQueue.main.sync {
                        completion(messageReponse)
                    }
                }catch let err{
                    print("error catch",err)
                }
          
            }.resume()
    }
    
    //Update Data 
    func putMethod(id: String, articleUpload: UploadData, completion: @escaping(MessageReponse)->()) {
        
            guard let url = URL(string: "\(baseURL)/\(id)") else {
                print("Error: cannot create URL")
                return
            }
            // Convert model to JSON data
            guard let jsonData = try? JSONEncoder().encode(articleUpload) else {
                print("Error: Trying to convert model to JSON data")
                return
            }
            
            // Create the request
            var request = URLRequest(url: url)
            request.httpMethod = "PATCH"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            URLSession.shared.dataTask(with: request) { data, response, error in
                guard error == nil else {
                    print("Error: error calling PUT")
                    print(error!)
                    return
                }
                guard let data = data else {
                    print("Error: Did not receive data")
                    return
                }
                guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                    print("Error: HTTP request failed")
                    return
                }
                do{
                    let messageReponse = try JSONDecoder().decode(MessageReponse.self, from: data)
                    DispatchQueue.main.sync {
                        completion(messageReponse)
                    }
                }catch let err{
                    print("error catch",err)
                }
            }.resume()
        }
    func uploadImage(image: UIImage, completion: @escaping(ImageReponse)->()) {
        let url = URL(string: "http://110.74.194.124:3000/api/images")

        // generate boundary string using a unique per-app string
        let boundary = UUID().uuidString

        let session = URLSession.shared

        // Set the URLRequest to POST and to the specified URL
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = "POST"

        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        var data = Data()

        // Add the image data to the raw http request data
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"image\"; filename=\".png\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(image.pngData()!)

        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)

        // Send a POST request to the URL, with the data we created earlier
        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: responseData!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print("Upload",json)
                }
            }
            do{
                let imgReponse = try JSONDecoder().decode(ImageReponse.self, from: responseData!)
                DispatchQueue.main.sync {
                    completion(imgReponse)
                }
            }catch let err{
                print("error catch",err)
            }
        }).resume()
    }
}
