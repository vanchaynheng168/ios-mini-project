//
//  ViewModel.swift
//  MINI-PROJECT-IOS
//
//  Created by Nheng Vanchhay on 12/16/20.
//

import Foundation
import UIKit
struct ViewModel {
    static var shared = ViewModel()
    var network =  Service.shared
    //fetch article
    func fetchData (title: String,fetchPage: Int,completion: @escaping ([ArticleModel],[Int])->()) {
        network.fetchArticle(title: title, fetchMorePage: fetchPage, completion: { (article, error) in
            if error != nil {
                print("data article error: ",error!)
                return
            }
            var articleModel = [ArticleModel]()
            for data in article.data {
                articleModel.append(ArticleModel(article: data))
            }
//            print("aretri,",article)
//            print("model Page: ",article.totalPage)
            completion(articleModel, [article.totalPage!, article.page!])
        })
    }
    //fun post data
    func postMethod(image: String, title: String, description: String, completion: @escaping(MessageReponse)->()) {
        network.postMethod(articleUpload: UploadData(title: title, description: description, published: true, image: image), completion: { (message) in
            completion(message)
        })
    }
    //fun Update Data
    func putMethod(id: String,image: String, title: String, description: String, completion: @escaping(MessageReponse)->()) {
        network.putMethod(id: id, articleUpload: UploadData(title: title, description: description, published: true, image: image), completion: { (message) in
            completion(message)
        })
    }
}
